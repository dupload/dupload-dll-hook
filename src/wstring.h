#pragma once

#include <string>

class dwstring : public std::wstring
{
public:
	dwstring() : std::wstring() { }
	dwstring( const wchar_t *wstr ) : std::wstring( wstr ) { }
	dwstring( const std::wstring &wstr ) : std::wstring( wstr ) { }

	bool isScreenshotFile();
	bool endsWith( dwstring const &ends );
	dwstring end(); 
};
