#pragma once

#include <windows.h>

typedef BOOL ( WINAPI *pCloseHandle )( HANDLE hObject );
BOOL WINAPI fCloseHandle( HANDLE hObject );

extern pCloseHandle oCloseHandle;
extern bool isWatching;
extern time_t lastUpdate;
