#pragma once

#include <windows.h>
#include "dUploadDll.h"

extern HHOOK cbtHook;

DLLEXPORT void hookInstall();
DLLEXPORT void hookUninstall();

LRESULT CALLBACK CBTHook( int nCode, WPARAM wParam, LPARAM lParam );
