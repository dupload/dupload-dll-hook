#include "hooks.h"

#pragma data_seg( ".dUploadDllShared" )
	HHOOK cbtHook = NULL;
#pragma data_seg()

DLLEXPORT void hookInstall()
{
	cbtHook = SetWindowsHookEx( WH_CBT, ( HOOKPROC )CBTHook, g_hInstance, 0 );
}

DLLEXPORT void hookUninstall()
{
	if ( UnhookWindowsHookEx( cbtHook ) )
	{
		cbtHook = NULL;
	}
}

LRESULT CALLBACK CBTHook( int nCode, WPARAM wParam, LPARAM lParam )
{
	if ( nCode < 0 )
		return CallNextHookEx( cbtHook, nCode, wParam, lParam );


    return CallNextHookEx( cbtHook, nCode, wParam, lParam );
}
