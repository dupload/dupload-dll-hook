#include <windows.h>

#include "CloseHandle.h"
#include "MinHook.h"
#include "hooks.h"

HINSTANCE g_hInstance = NULL;

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved )
{
    switch( fdwReason ) 
    { 
        case DLL_PROCESS_ATTACH:
		{
			g_hInstance = hinstDLL;
			DisableThreadLibraryCalls( g_hInstance );

			if ( !updateStatus() )
			{
				return FALSE;
			}

			MH_Initialize();
			MH_CreateHook( &CloseHandle, &fCloseHandle, reinterpret_cast< void** >( &oCloseHandle ) );
			MH_EnableHook( &CloseHandle );

            break;
		}

		case DLL_PROCESS_DETACH:
		{
			MH_Uninitialize();

            break;
		}

        case DLL_THREAD_ATTACH:
            break;

        case DLL_THREAD_DETACH:
            break;
    }

    return TRUE;
}
