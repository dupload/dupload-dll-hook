#include "wstring.h"

bool dwstring::isScreenshotFile()
{
	return ( endsWith( L".jpg" ) || endsWith( L".jpeg" ) || endsWith( L".png" ) );
}

bool dwstring::endsWith( dwstring const &ends )
{
	if ( length() >= ends.length() )
	{
		return ( compare( length() - ends.length(), ends.length(), ends ) == 0 );
	}
	else
	{
		return false;
	}
}

dwstring dwstring::end()
{
	std::wstring::size_type found = find_last_of( L"\\" );

	if ( found != npos )
	{
		return substr( found + 1, npos );
	}
	else
	{
		return dwstring();
	}
}
