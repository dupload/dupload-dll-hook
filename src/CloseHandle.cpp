#include "CloseHandle.h"
#include "hooks.h"
#include "wstring.h"

pCloseHandle oCloseHandle = NULL;
bool isWatching = false;
time_t lastUpdate = 0;

BOOL WINAPI fCloseHandle( HANDLE hObject )
{
	if ( lastUpdate == 0 || lastUpdate < updateTime )
	{
		updateStatus();
		lastUpdate = updateTime;
	}

	if ( isWatching )
	{
		TCHAR filePath[ MAX_PATH ];
		DWORD dwRet = GetFinalPathNameByHandle( hObject, filePath, MAX_PATH, VOLUME_NAME_DOS );

		if ( dwRet < MAX_PATH )
		{
			dwstring file( filePath );

			if ( file.isScreenshotFile() )
			{
				COPYDATASTRUCT data = {
					NULL,
					sizeof( filePath ),
					&filePath
				};

				SendMessage( mainWindow, WM_COPYDATA, NULL, ( LPARAM )&data );

				OutputDebugString( L"fCloseHandle" );
			}
		}
	}

    return oCloseHandle( hObject );
}
