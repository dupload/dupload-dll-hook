#include <ctime>
#include <string.h>

#include "CloseHandle.h"
#include "dUploadDll.h"
#include "hooks.h"
#include "wstring.h"

#pragma data_seg( ".dUploadDllShared" )
	time_t updateTime = 0;
	int watchingAppsCount = 0;
	std::array< std::array< wchar_t, MAX_PATH >, MAX_WATCHING_APPS > watchingApps = { };

	HWND mainWindow = 0;
#pragma data_seg()

DLLEXPORT void addWatchingApp( const wchar_t *app )
{
	if ( watchingAppsCount + 1 == MAX_WATCHING_APPS )
		return;

	wcsncpy_s( watchingApps[ watchingAppsCount ].data(), watchingApps[ watchingAppsCount ].size(), app, MAX_PATH );
	watchingAppsCount++;

	updateTime = time( NULL );
}

DLLEXPORT void clearWatchingApps()
{
	watchingAppsCount = 0;

	updateTime = time( NULL );
}

bool updateStatus()
{
	if ( cbtHook != NULL )
	{
		WCHAR processPath[ MAX_PATH ];
		DWORD dwRet = GetModuleFileName( NULL, processPath, MAX_PATH );

		if ( dwRet < MAX_PATH )
		{
			isWatching = false;

			dwstring processName = dwstring( processPath ).end();
			for ( int i = 0; i < watchingAppsCount; i++ )
			{
				if ( processName == dwstring( watchingApps[i].data() ) )
				{
					isWatching = true;
					break;
				}
			}
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}

DLLEXPORT void setHWND( HWND hwnd )
{
	mainWindow = hwnd;
}
