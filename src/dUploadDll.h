#pragma once

#include <array>
#include <string>
#include <Windows.h>

#define DLLEXPORT extern "C" __declspec( dllexport )
#pragma comment( linker, "/SECTION:.dUploadDllShared,RWS" )

#define MAX_WATCHING_APPS 32

extern HINSTANCE g_hInstance;

extern time_t updateTime;
extern int watchingAppsCount;
extern std::array< std::array< wchar_t, MAX_PATH >, MAX_WATCHING_APPS > watchingApps;

extern HWND mainWindow;

DLLEXPORT void addWatchingApp( const wchar_t *app );
DLLEXPORT void clearWatchingApps();

bool updateStatus();

DLLEXPORT void setHWND( HWND hwnd );
