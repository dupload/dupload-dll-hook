SET( MinHook_PATH "${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/MinHook" )

file( GLOB MinHook_INCLUDES "${MinHook_PATH}/src/*.h" )
file( GLOB MinHook_SOURCES "${MinHook_PATH}/src/*.cpp" )

LIST( APPEND MinHook_INCLUDES "${MinHook_PATH}/MinHook.h" )

if( CMAKE_CL_64 )
    file( GLOB MinHookHde_INCLUDES "${MinHook_PATH}/src/hde64/*.h" )
    file( GLOB MinHookHde_SOURCES "${MinHook_PATH}/src/hde64/*.c" )
    
    include_directories( "${MinHook_PATH}/src/hde64/" )
else()
    file( GLOB MinHookHde_INCLUDES "${MinHook_PATH}/src/hde32/*.h" )
    file( GLOB MinHookHde_SOURCES "${MinHook_PATH}/src/hde32/*.c" )
    
    include_directories( "${MinHook_PATH}/src/hde32/" )
endif()

LIST( APPEND dUploadDll_INCLUDES ${MinHook_INCLUDES} ${MinHookHde_INCLUDES} )
LIST( APPEND dUploadDll_SOURCES ${MinHook_SOURCES} ${MinHookHde_SOURCES} )

include_directories( "${MinHook_PATH}/" )
include_directories( "${MinHook_PATH}/src/" )
